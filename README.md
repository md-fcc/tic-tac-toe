My tic-tac-toe game to fulfill the [FreeCodeCamp challenge](https://www.freecodecamp.org/challenges/build-a-tic-tac-toe-game).

Live website: [Tic-Tac-Toe](https://md-fcc.gitlab.io/tic-tac-toe/)

The Javascript code uses 3 classes:
- Player - holds data about each player, AI logic, etc
- Board - holds data about who owns each cell on the board, and logic for checking the board state (win/loss/tie)
- Game - contains the logic for starting a new game and handling player/AI input
- GameView - responsible for updating HTML elements related to the game

The code also features the negamax recursive algorithm for an  essentially unbeatable AI opponent.

Some of the code seems over-engineered. For example, tic-tac-toe is a two player game but I implemented this as an array of Players. So you will see code using the array like "players[0]" implying the ability for an unlimited players, but also see code implying 2 players (for example in Game.switchPlayer(), or in the GameView code, or in the Game constructor function). Regardless of the architecture, I learned a lot about Javascript classes and thinking about how to encapsulate code.
