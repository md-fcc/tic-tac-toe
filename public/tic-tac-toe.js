// -----------------------------------------------------------------------------
// Classes ---------------------------------------------------------------------
// -----------------------------------------------------------------------------

class Player {
	// Class contains basic player data (name, mark: "X or O", win-loss record, etc)
	// also contains methods for determining the next best move (used for AI opponent)
	
    constructor({id, name, mark, isHuman}) {
        this.id = id;
        this.name = name;
        this.mark = mark;
        this.isHuman = isHuman;
        this.wins = 0;
        this.losses = 0;
        this.ties = 0;
		this.opponentID = 0;
    }

    resetWLRecord() {
		// sets win-loss record to 0-0-0
        this.wins = 0;
        this.losses = 0;
        this.ties = 0;
    }
    
    
    negamax(board, i, j) {
        // helper function to call negamax algorithm with additional
        // parameters needed for recursion.
        
        let depthLimit = 9,
            nextPlayer = this.opponentID,
            sign = -1;

        let result = -this.negamax_recursion(board, i, j, depthLimit, nextPlayer, sign);
        return(result);
    }

    negamax_recursion(board, i, j, depth, player, sign) {
        // negamax recursive algorithm to score a game outcome by alternating players,
        // assuming each player makes a move in their best interest.
        // Parameters:
        //     board: current board state
        //     i, j:  board cell location of last move
        //     depth: initial recurisve call limits the depth search to this many levels
        //     player: indicates who is making the trial move
        //     sign: the negamax algorithm alternates -1/+1 to take advantange of 
        //           max(a, b) = −min(−a, −b). Max outcome for 1 player is the min outcome
        //                                     for the other player.
        
        // Check end result (if any) of the current board state
        let boardState = board.checkEndCondition(i, j);
        if (boardState != -1) {
            if (boardState == -99) { return(9 - depth) }; // Tie
            if (boardState == this.id) { return(sign * (100 + depth)) }; // Maximizing Player Wins
            if (boardState == this.opponentID) { return(sign * (-100 - depth)) }; // Minimizing Player Wins
        } else if (depth == 0) {
            return(9 - depth); // reached depth limit
        }
    
        let bestResult = -9999;
        let openCells = board.findAvailableMoves();
        
        // try each available move
        for (let move = 0; move < openCells.length; move++) {
            let i = openCells[move][0],
                j = openCells[move][1];
            
            // make trial move
            board.cell[i][j] = player;
            board.openCells -= 1;
            
            // Switch Player
            let nextPlayer;
            if (player == this.opponentID) {
                nextPlayer = this.id;
            } else {
                nextPlayer = this.opponentID;
            }
    
            // Get game outcome for the trial move
            let trialResult = -this.negamax_recursion(board, i, j, depth - 1, nextPlayer, -sign);
            bestResult = Math.max(bestResult, trialResult);
            
            //undo trial move
            board.cell[i][j] = -1;
            board.openCells += 1;
        }
        return(bestResult);
    }

    getRandomInt(min, max) {
        // returns a random integer in the range [min, max], inclusive
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };
	
	findBestMove(board) {
		// Finds the best available move based on the board state.
		// returns a cell position of the best move, e.x. [0, 1]
		// Assumes each player also makes the best move in their own interest.
		// Handles the opening move via hard-coding
		// the strategy to use the center or corner cells.
		
		let possibleMoves = board.findAvailableMoves();
		let possibleResults = [];
        
        // best first move is choosing the center or corners
        // should probably find a way to not hard-code these indices
        if (possibleMoves.length == 9) {
            let firstMoveIndices = [0, 2, 4, 6, 8]
            let i = this.getRandomInt(0, firstMoveIndices.length - 1);
            return(possibleMoves[firstMoveIndices[i]]);
        }

		// try all moves, score the game outcome for each move
		for (let move = 0; move < possibleMoves.length; move++) {
			let i = possibleMoves[move][0], 
				j = possibleMoves[move][1];
            
            // make trial move
            board.cell[i][j] = this.id;
            board.openCells -= 1;
            
            // Get game outcome for the trial move
            let result = this.negamax(board, i, j);
            possibleResults.push(result);
            
            //undo trial move
            board.cell[i][j] = -1;
            board.openCells += 1;
		}
		
        //loop through possible results, return best (highest score)
        let maxResult = Math.max.apply(null, possibleResults);

		let bestMoveIndices = [];
		for (let i = 0; i < possibleResults.length; i++) {
			if (possibleResults[i] == maxResult) {
				bestMoveIndices.push(i);
			}
		}

        if (bestMoveIndices.length == 1) {
            return(possibleMoves[bestMoveIndices[0]]);
        } 

        let i = this.getRandomInt(0, bestMoveIndices.length - 1);
        return(possibleMoves[bestMoveIndices[i]]);
		
	}
};


class Board {
	// class defining the game board and current ownership of each cell within the board.
	// cell ownership is indicated by the cell containing the owning player ID,
	// or -1 if the cell is open (not owned by any player)
	
    constructor() {
		// initialize board state to have all cells open, -1
        this.width = 3;
        this.openCells = 9;
        this.cell = [[-1, -1, -1],
                     [-1, -1, -1],
                     [-1, -1, -1]];
    }
	
	
    findAvailableMoves() {
		// returns a list of open cells not owned by any player	
        let availableMoves = [];
        for (let i = 0; i < 3; i++) {
            for (let j = 0; j < 3; j++) {
                if (this.cell[i][j] == -1) {
                    availableMoves.push([i, j]);
                }
            }
        }
        return availableMoves;
    }

	
    rowWin(row) {
		// returns true if all cells in the passed row are owned by a sigle player
    	for (let j = 0; j < this.width; j++) {
            if (this.cell[row][j] != this.cell[row][0]) {
              return(false);
            }
        }
        return(true);
    }

	
    colWin(col) {
		// returns true if all cells in the column are owned by a sigle player
    	for (let i = 0; i < this.width; i++) {
            if (this.cell[i][col] != this.cell[0][col]) {
              return(false);
            }
        }
        return(true);
    }

	
    diagLeftWin() {
		// returns true if all cells on the left diagonal are owned by a sigle player
		// example:
		// X _ _
		// _ X _
		// _ _ X
    	for (let i = 0; i < this.width; i++) {
            if (this.cell[i][i] != this.cell[0][0]) {
              return(false);
            }
        }
        return(true);
    }

	
    diagRightWin() {
		// returns true if all cells on the left diagonal are owned by a sigle player
		// example:
		// _ _ X
		// _ X _
		// X _ _
    	for (let i = 0; i < this.width; i++) {
            if (this.cell[i][(this.width - 1) - i] != 
                this.cell[0][this.width - 1])
            {
              return(false);
            }
        }
        return(true);
    }

    checkEndCondition (i, j) {
		// checks to see if the input cell [i, j] indicates the end of the game,
		// either win, loss, or tie.
		// returns the winning player id if a player won,
		// returns -99 if there is a tie
		// returns -1 if the game is not over yet
		
        if (this.rowWin(i) || this.colWin(j) || 
            ((i + j == (this.width - 1)) && this.diagRightWin()) || 
            ((i == j) && this.diagLeftWin()))
        {
            return(this.cell[i][j]); // Win for player owning cell[i][j]
        } else if (this.openCells == 0) {
            return(-99); // tie
        } else {
            return(-1) // game not over yet
        }
    }
}


class Game {
	// Class contains the game logic
	// emits events so that other objects can react to changes in the game state,
	// e.x. GameView can update HTML after game emits a state change.
	
    constructor({
        player_0_name,
        isPlayer_0_human,
        player_0_mark,
        player_1_name,
        isPlayer_1_human,
        player_1_mark,
        isPlayer_0_starting
    }) {
        this.board;
		this.players = [];
        
        this.maxBotVsBotRounds = 100; // limit maximum number of iterations on computer vs computer games
        this.rounds = 0;

        this.players.push(new Player({id: 0, name: player_0_name, mark: player_0_mark, isHuman: isPlayer_0_human}))
        this.players.push(new Player({id: 1, name: player_1_name, mark: player_1_mark, isHuman: isPlayer_1_human}))
        
		this.players[0].opponentID = 1;
		this.players[1].opponentID = 0;

        let startingPlayer = 0;
        if (!isPlayer_0_starting) {
            startingPlayer = 1;
        }

		this.activePlayerID = startingPlayer;
        this.startNewGame();
    }
    
    switchPlayer() {
		// switches from one player to the next and moves the HTML posession arrow
        if (this.activePlayerID == this.players[0].id) {
            this.activePlayerID = this.players[1].id;
			$(this).triggerHandler("pos-arrow-changed", 0);
            
        } else {
            this.activePlayerID = this.players[0].id;
			$(this).triggerHandler("pos-arrow-changed", 1);
        }
		
    }

    endTurn(i, j) {
		// handles what should happen at the end of a turn, based on the last
		// move location [i, j]
		
        let winStatus = this.board.checkEndCondition(i, j);
        if (winStatus != -1) {
			// game has ended
			
            if (winStatus == -99) {
                alert("Tie!")
            } else {
                alert(this.players[winStatus].name + " wins!")
            }
			
            this.gameActive = false;
            this.updateWLRecord(winStatus);
            this.rounds++;
			
            if (!(this.players[0].isHuman || this.players[1].isHuman) &&
                this.rounds < this.maxBotVsBotRounds) {
                this.startNewGame();
            } else {
                this.startNewGame();
            }
        } else {
			// game is not over, continue playing
            this.switchPlayer();
            if (!this.players[this.activePlayerID].isHuman) {
                this.computerMove();
            }
        }
    }

    updateWLRecord(winningPlayerID) {
		// updates the win-loss record of each player.
        if (winningPlayerID == -99) {  // handles tie condition
            for (let player of this.players) {
                player.ties += 1;
            }
        } else {  // handles win/loss
            for (let player of this.players) {
                if (player.id == winningPlayerID) {
                    player.wins += 1;
                } else {
                    player.losses += 1;
                }
            }
        }
		$(this).triggerHandler("WLRecordUpdated");
    }

    computerMove() {
		// allows an AI player to make a move
        let bestMove = this.players[this.activePlayerID].findBestMove(this.board);
        this.playerMove(bestMove[0], bestMove[1]);
        this.endTurn(bestMove[0], bestMove[1]);
    }

    playerMove(i, j) {
		// allows a player to own cell [i, j]
        this.board.cell[i][j] = this.activePlayerID;
		this.board.openCells -= 1;

		$(this).triggerHandler(
			"player-moved", 
			[i, j, this.players[this.activePlayerID].mark]
		);
    }

    cellClick (i, j) {
		// handles user clicking on a gameboard cell
        if (this.gameActive &&
            this.players[this.activePlayerID].isHuman &&
            this.board.cell[i][j] == -1)
        {
            this.playerMove(i, j);
            this.endTurn(i, j);
        }
    }

    startNewGame() {
        this.board = new Board();
        this.gameActive = true;

		$(this).triggerHandler("start-new-game");

        if (!this.players[this.activePlayerID].isHuman) {
            this.computerMove();
        }
    }
};


class GameView {
	// responsible for updating the HTML based on the game state
	
	constructor(game) {
		this.game = game;
		
		// listen to events emitted from the game object
		$(this.game).on( "WLRecordUpdated", () => {this.showWLRecord()} );
		
		$(this.game).on( "pos-arrow-changed", 
			(event, playerID) => {this.movePossessionArrow(playerID)} );
		
		$(this.game).on("player-moved", 
			(event, i, j, mark) => {this.markCell(i, j, mark)});
		
		$(this.game).on("start-new-game", () => {this.resetCells()});
	}
	
	showWLRecord() {
		for (let player of this.game.players) {
			$("#p"+ player.id + "-WLRecord")
				.html(player.wins + "-" + player.losses + "-" + player.ties)
		}
	}
	
	movePossessionArrow(playerID) {
		if (playerID == 0) {
			$("#pos-arrow").html("  Possession ►");
		} else {
			$("#pos-arrow").html("◄ Possession  ");
		}
	}
	
	markCell(i, j, mark) {
        if (mark === "X") {
            $("#cell" + i + j).attr("src", "img/square-x.png");
        } else {
            $("#cell" + i + j).attr("src", "img/square-o.png");
        }		
	}
	
	resetCells() {
	    for (let i = 0; i < this.game.board.width; i++) {
            for (let j = 0; j < this.game.board.width; j++) {
                $("#cell" + i + j).attr("src", "img/square-empty.png");
            }
        }
	}
	
	resetWLRecord() {
		$("#p0-WLRecord").html("0-0-0");
        $("#p1-WLRecord").html("0-0-0");
	}
	
};

// -----------------------------------------------------------------------------

$(document).ready(function () {
    
    // -------------------------------------------------------------------------
    // ------------------------------- MAIN ------------------------------------
    // -------------------------------------------------------------------------
    
    let setupMode = true;  // begin in setup mode where users can change game options
    
    // declare variables to hold game objects. These are instantiated in:
    // $("#startOrRestGame").click(function(){...});
    let game = null;
	let gameView = null;
    
    // -------------------- Helper functions------------------------------------
    toggleSetupInputs = function() {
        // enables/disables HTML elements for setting up a game
        $("[name^=p0]").prop('disabled', function(index, value) { return !value; });
        $("[name^=p1]").prop('disabled', function(index, value) { return !value; });
        $("[name=startingPlayer]").prop('disabled', function(index, value) { 
                return !value;
            });
    };

    getPlayerName = function(obj) {
        // returns the name of a player, or a default name if none is defined
        let name = obj.val();

        if (name == '') {
            if (obj.attr("name") == 'p0-name') {
                name = 'Player 1';
            } else {
                name = 'Player 2';
            }
        }
        
        return(name);
    };
    
    
    // -------------------------------------------------------------------------
	// Define HTML click handlers ----------------------------------------------
	// -------------------------------------------------------------------------
	
    // Functionality for Start Game Button
    $("#startOrRestGame").click(function() {
        if (setupMode) {

            toggleSetupInputs();
            $("#startOrRestGame").html("Reset Match");
            setupMode = false;

            game = new Game({
                player_0_name:       getPlayerName($("input[name='p0-name']")),
                isPlayer_0_human:    $('input[name="p0-human"][value="human"]').prop('checked'),
                player_0_mark:       $("input[name='p0-optXO']:checked").val(),
                player_1_name:       getPlayerName($("input[name='p1-name']")),
                isPlayer_1_human:    $('input[name="p1-human"][value="human"]').prop('checked'),
                player_1_mark:       $("input[name='p1-optXO']:checked").val(),
                isPlayer_0_starting: $('input[name="startingPlayer"][value="0"]').prop('checked')
            });

			gameView = new GameView(game);
			
        } else {
			// reset game

			gameView.resetCells();
			gameView.resetWLRecord();
			
            game = null;
			gameView = null;
			
            toggleSetupInputs();
            $("#startOrRestGame").html("Start Match");
            setupMode = true;
        }
    });

    // Prevent both players from being "X" or both being "O"
    // When one player chooses "X" set the other to "O", and vise versa 
    $("[name$='optXO']").click(function() {

        let player = $(this).attr("name");
        let mark = $(this).val();
        let oppPlayer = "";
        let oppMark = "";
        
        if (player == "p1-optXO") {
            oppPlayer = "p0-optXO"
        } else {
            oppPlayer = "p1-optXO"
        }

        if (mark == "X") {
            oppMark = "O"
        } else {
            oppMark = "X"
        }

        $('input[name="' + oppPlayer + '"][value="' + oppMark + '"]').prop('checked', true);
    });

    
    // Assign the possession based on the checked player radio button
    $("[name$='startingPlayer']").click(function() {
        let player = $(this).val();
        let checked = $(this).prop('checked');
        if (player == 0 && checked) {
            $("#pos-arrow").html("◄ Possession  ");
        } else {
            $("#pos-arrow").html("  Possession ►");
        }
    });


    // Name Text Input propogates to other html elements
    $("[name$=name]").keyup(function(){
        let player_number = 0;
        if ($(this).attr("name") == "p1-name") {
            player_number = 1;
        }

        let name = getPlayerName($(this));
        let message = name + ' starts'
        $("#p" + player_number + "-starts").text(message);
    });


    // Assign functions to tic-tac-toe board buttons
	// assumes 3x3 game board
    for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 3; j++) {
            $("#cell" + i + j).click(function() {
                if (game) {
                    game.cellClick(i, j);
                }
            });
        }
    }
});
